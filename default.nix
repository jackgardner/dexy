{ buildBazelPackage
, git
, go
, lib
, cacert
}:
buildBazelPackage rec {
  name = "dexy";
  version = "1.0.0";
  src = ./.;

  nativeBuildInputs = [ git go ];
  bazelTarget = "//cmd";

  fetchAttrs = {
    preBuild = ''
      sed -e 's:go_register_toolchains():go_register_toolchains(go_version = "host"):g' -i WORKSPACE
      sed -e 's:gazelle_dependencies():gazelle_dependencies(go_sdk = "host"):g' -i WORKSPACE
      # tell rules_go to invoke GIT with custom CAINFO path
      export GIT_SSL_CAINFO="${cacert}/etc/ssl/certs/ca-bundle.crt"
    '';

    preInstall = ''
      # Remove all built in external workspaces, Bazel will recreate them when building
      rm -rf $bazelOut/external/{bazel_tools,\@bazel_tools.marker}
      rm -rf $bazelOut/external/{embedded_jdk,\@embedded_jdk.marker}
      rm -rf $bazelOut/external/{go_sdk,\@go_sdk.marker}
      rm -rf $bazelOut/external/{local_*,\@local_*}
    '';

    sha256 = "lJRGMvvaLlXUT1QmO9KZ+C501nTJ8Eqxo/PjKO+Oaxg=";
  };

  buildAttrs = {
    preBuild = ''
       sed -e 's:go_register_toolchains():go_register_toolchains(go_version = "host"):g' -i WORKSPACE
       sed -e 's:gazelle_dependencies():gazelle_dependencies(go_sdk = "host"):g' -i WORKSPACE
    '';
    installPhase = ''
      install -Dm755 bazel-bin/cmd/cmd_/cmd $out/bin/dexy
    '';
  };

  meta = with lib; {
    homepage = https://gitlab.com/chronojam/dexy;
    description = "Dexy, a OIDC cli tool";
    platforms = platforms.all;
  };
}