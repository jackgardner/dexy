// Copyright © 2017 Calliope Gardner <calliope@chronojam.co.uk>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package config

import (
	"testing"
	"github.com/stretchr/testify/assert"
)

func TestWith(t *testing.T) {
	t.Run("ReplaceNilValueIfPresent", func(t *testing.T) {
		c := New("client_id", "client_secret", "callback_url", 10000, "", []string{}, "")
		n := c.With(New("", "", "", 10000, "host", []string{}, ""))
		assert.NotNil(t, n.ClientID)
		assert.NotNil(t, n.ClientSecret)
		assert.NotNil(t, n.CallbackURL)
		assert.NotNil(t, n.Host)
	})
	t.Run("ReplaceValueIfPresent", func(t *testing.T) {
		c := New("client_id", "client_secret", "callback_url", 10000, "host", []string{}, "")
		n := c.With(New("", "", "", 10000, "host2", []string{}, ""))
		assert.Equal(t, *n.Host, "host2", "")
	})
	t.Run("ReplaceValueIfPresentMultiple", func(t *testing.T) {
		c := New("", "", "callback_url", 10000, "", []string{}, "")
		n := c.With(&Config{}).
				With(&Config{}).
				With(
					New("client_id", "client_secret", "", 10000, "host", []string{}, ""),
				)
		assert.Equal(t, n.CallbackURL(), "http://callback_url:10000/oauth2/callback", "")
	})
}

func TestRead(t *testing.T) {
	t.Run("TestNoErrorOnFileMissing", func(t *testing.T) {
		c := New("", "", "", 10000, "", []string{}, "")
		n := c.Read("/a/file/that/does/not/exist.yaml")

		assert.Nil(t, n.ClientID)
	})
}